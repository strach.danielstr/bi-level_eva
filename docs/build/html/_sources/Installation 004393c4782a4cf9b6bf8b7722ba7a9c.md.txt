# Installation

### Clone Repository

`git clone --recurse-submodule git@git-ce.rwth-aachen.de:ineed-dc/ineed-dc-framework.git`

### Install Environment

A collection of all required packages is given by the Anaconda environments defined in `envINEED_win.yml` and `envINEED_linux.yml`. To create a local environment from file execute:

`conda env create -f envINEED_win.yml`

or for Linux operating systems:

`conda env create -f envINEED_linux.yml`

### Update Environment

Errors may occur when the local environment is not up-to-date. To update the local environment from `envINEED_win.yml` or `envINEED_linux.yml` execute:

`conda activate envINEED
conda env update --file envINEED_win.yml`

or for linux operating systems:

`conda activate envINEED
conda env create --file envINEED_linux.yml`

### Update the yml file

If you have installed new packages during the development and excution of the program, you should think of updating the yml environment file in both windows and linux systems (or at least contact Jingyu or Felix regarding this) as well. To export the new environment.yml file, excute:

`conda activate envINEED
conda env export > envINEED_win.yml`

or for linux operating systems:

`conda activate envINEED
conda env export > envINEED_linux.yml`

### Install Optimizing Solvers

The applied modeling language Pyomo supports a wide variety of solvers. Pyomo has specialized interfaces to some solvers (for example, BARON, CBC, CPLEX, and Gurobi). It also has generic interfaces that support calling any solver that can read AMPL “.nl” and write “.sol” files and the ability to generate GAMS-format models and retrieve the results. You can get the current list of supported solvers using the pyomo command:

`pyomo help --solvers`

Three solvers are recommended by the developers of this programm, Gurobi, glpk, CBC and ipopt. Gurobi glpk and CBC are used in preference when try to solve a LP problem. Although Gurobi usually works more efficient than glpk anc CBC, it requires a commercial license. The solver Ipopt can be applied when the problem is non-linear. The solvers are not installed by default in the environment envINEED. Thus, execute the following command to install the solvers by need:

**GLPK (GNU Linear Programming Kit)**

`conda install -c conda-forge glpk`

**CBC (Coin-or branch and cut)**
The easiest way to use CBC is to download its stand-alone executable and install/put it in your local environment (envs/envINEED/bin). The download page is: [https://ampl.com/products/solvers/open-source/#cbc](https://ampl.com/products/solvers/open-source/#cbc).

**Ipopt (Interior Point Optimizer)**
You can also download ipopt executable for direct use from the website [https://ampl.com/products/solvers/open-source/#ipopt](https://ampl.com/products/solvers/open-source/#ipopt). Alternatively, you can run the following command in terminal to install ipopt package with conda:

`conda install -c conda-forge ipopt=3.11.1`

To use **Gurobi** you should first request a license on [https://www.gurobi.com/downloads/](https://www.gurobi.com/downloads/). To install the license on a computer where Gurobi Optimizer is installed, copy and paste the command (You can find it on the license page.) to the Start/Run menu (Windows only) or a command/terminal prompt (any system):

`grbgetkey xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`

### Executing Prosumer Model

`python3 runme.py`