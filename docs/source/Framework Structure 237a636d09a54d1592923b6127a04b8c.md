# Framework Structure

The project strcuture represents the structure of the developed prosumer model. The different parts and their internal structure is described in the following subsections:

### Main Folder

The main folder contains the executive scripts, environment files as well as storage folders for simulation in- and outputs:

- `main/`
    - `runme.py` : Main script to build a prosumer model and run the optimization process and its subfuctions
    - `envINEED.yml` : YAML-File defining the used Anaconca environment, i.e. required packages
    - `input_files/` : Storage for input files
    - `output_files/` : Storage for simulation results

### Prosumer Library

The prosumer library is a supporting model database with prosumer models of different prosumer categories and their typical parameters. The prosumer library has been devided into two separate parts:

- `prosumer_library/`
    - `prosumer_models/` : This directory includes prosumer classes with different regulatory conditions. An algebraic optimization function set can be derived from a complete prosumer model. Therefore, the prosumers can be modified and optimized independently according to different technical and strategical conditions. A prosumer model has defined interfaces with other prosumer models and the supply network
    - `prosumer_database/` : This directory includes the parameterization options for the prosumer models.

### Component Library

The component library is also a supporting model database for prosumer modeling, which provides physical mathematical descriptions of commonly used electrical and thermal components and typical customary parameterization options for them. The prosumer model is incomplete without components and therefore not able to be simulated.

- `component_library/`
    - `component_models/` : This directory includes the component models. These models are written in modeling language pyomo and the physical properties of each component are described as mathematical algebraic expressions (constraints) using the gramma of pyomo. These expressions are indispensible for the optimization function set.
    - `component_database/` : This directory includes the the parameters for the component models.
    - `BaseComponent.py` : This python class includes fundamental attributes, functions and constraints of different components except EMS component. All components except EMS should be established based on BaseComponent. The UML-Diagram `InEEd-DC_Components.drawio` shows the relationship between different component models.

### Tools

To enrich the functionality of the product, we have created an extensive tool-kit which supports the prosumer modeling and optimizing procedure. In latest version, prediction generator, electrical profile generator are available. In future updates, more tools will be provided.