# FOCUS-Framework
The FOCUS-Framework (Framework for Optimizing sector-Coupled Urban energy Systems) represents a modeling library that is designed to decarbonize Prosumer applications and sector-coupled urban energy systems. 
The core model features a three-level structure in order to optimize entire city energy systems. 
The three levels are prosumer, quarter, and city level. With its bottom-up approach, starting at the Prosumer level, complex structures can be approximated. 
This allows the user to analyze the impact of prosumer behavior on the upper city level. 
Different behavior patterns can originate from the electrification of heat demand, the use of demand side management, or applying electric mobility with bidirectional charging. <br>
**This project is constantly under development. As of now it is advised to use the Prosumer layer only.**

[![Documentation Status](https://www.fenaachen.net/projekte/fen-ineed-dc)](https://www.fenaachen.net/projekte/fen-ineed-dc)
[![DOI]()]()

## Features

  * The FOCUS-Framework is a mixed-integer linear optimization toolbox for sector-coupled energy systems
  * Multi-objective optimization is used to minimize costs as well as CO2-emissions
  * Optimization mode finds optimal capacity expansion of desired technologies
  * Operation mode with a rolling horizon approach is used to realistically simulate the energy system
  * Any sector-coupled prosumer application can be approximated
  * City quarters and energy communities with multiple prosumers can be flexibly modeled
  * Component database includes a comprehensive technology portfolio

## Further Information

- [Model Usage](#model-usage)
  - [Clone Repository](#clone-repository)
  - [Install Environment](#install-environment)
  - [Update Environment](#update-environment)
  - [Update the yml file](#update-the-yml-file)
  - [Install optimizing solvers](#install-optimizing-solvers)
  - [Executing Prosumer Model](#executing-prosumer-model)
- [Contribution Guidelines](#contribution-guidelines)
  - [Branches](#branches)
  - [Merge into main branch](#merge-into-main-branch)
  - [Environment File](#environment-file)
  - [Modeling Language](#modeling-language)
  - [Programming guidelines and Coding Style](#programming-guidelines)
- [Project Structure](#project-structure)
  - [Main Folder](#main-folder)
  - [Prosumer Library](#prosumer-library)
  - [Component Library](#component-library)
  - [Tools](#tools)


## Model Usage
### Clone Repository
```
git clone --recurse-submodule git@git-ce.rwth-aachen.de:ineed-dc/ineed-dc-framework.git
```
### Install Environment
A collection of all required packages is given by the Anaconda environments defined in `envINEED_win.yml` (Windows based systems) and `envINEED_linux.yml` (Linux based systems). To create a local environment from file execute:
```
conda env create -f envINEED_win.yml
```

or for Linux operating systems:
```
conda env create -f envINEED_linux.yml
```
### Update Environment
Errors may occur when the local environment is not up-to-date. To update the local environment from `envINEED_win.yml` or `envINEED_linux.yml` execute:
```
conda activate envINEED
conda env update --file envINEED_win.yml
```
or for linux operating systems:
```
conda activate envINEED
conda env create --file envINEED_linux.yml
```
### Update the yml file
If you have installed new packages during the development and excution of the program, you should think of updating the yml environment file in both windows and linux systems (or at least contact Jingyu or Felix regarding this) as well. To export the new environment.yml file, excute:
```
conda activate envINEED
conda env export > envINEED_win.yml
```
or for linux operating systems:
```
conda activate envINEED
conda env export > envINEED_linux.yml
```
### Install Optimizing Solvers
The applied modeling language Pyomo supports a wide variety of solvers. Pyomo has specialized interfaces to some solvers (for example, BARON, CBC, CPLEX, and Gurobi). It also has generic interfaces that support calling any solver that can read AMPL “.nl” and write “.sol” files and the ability to generate GAMS-format models and retrieve the results. You can get the current list of supported solvers using the pyomo command:
```
pyomo help --solvers
```

Three solvers are recommended by the developers of this programm, Gurobi, glpk, CBC and ipopt. Gurobi glpk and CBC are used in preference when try to solve a LP problem. Although Gurobi usually works more efficient than glpk anc CBC, it requires a commercial license. The solver Ipopt can be applied when the problem is non-linear. The solvers are not installed by default in the environment envINEED. Thus, execute the following command to install the solvers by need:

**GLPK (GNU Linear Programming Kit)**
```
conda install -c conda-forge glpk
```

**CBC (Coin-or branch and cut)**
The easiest way to use CBC is to download its stand-alone executable and install/put it in your local environment (envs/envINEED/bin). The download page is: https://ampl.com/products/solvers/open-source/#cbc.

**Ipopt (Interior Point Optimizer)**
You can also download ipopt executable for direct use from the website https://ampl.com/products/solvers/open-source/#ipopt. Alternatively, you can run the following command in terminal to install ipopt package with conda:
```
conda install -c conda-forge ipopt=3.11.1
```

To use **Gurobi** you should first request a license on https://www.gurobi.com/downloads/. To install the license on a computer where Gurobi Optimizer is installed, copy and paste the command (You can find it on the license page.) to the Start/Run menu (Windows only) or a command/terminal prompt (any system):
```
grbgetkey xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```

### Executing Prosumer Model
```
python3 runme.py
```

## Contribution Guidelines
If you want to contribute to the prosumer model, please follow the rules and guidelines specified below:
### Branches
- All developing works should be done in personal branchs or feature branches. Please follow the name guidelines for creating new branches: `dev_<NameAbbr>` or `dev_<feature>`.
- Merge requests to the `main` branch should be coordinated with other developers in advance.
- The main branch will be updated at least every two weeks.

### Merge into main branch
For the update process (mergen) of main branch, the following instruction in 7 Steps has been presented and should be followed:
| Step | discription |
| ------ | ------ |
| Step 1 | pull main branch (remote) |
| Step 2 | merge main branch (local) in MyFeature branch (local) and solve merge-conflicts in MyFeature branch |
| Step 3 | test the MyFeature branch (local) by its functionality |
| Step 4 | merge MyFeature branch (local) in main branch (local) |
| Step 5 | test the main branch (local) by its functionality |
| Step 6 | commit MyFeature branch (local) and push MyFeature branch to remote |
| Step 7 | submit a merge request from MyFeature branch (remote) to main branch (remote) |

Importance:
 - On principle, only functional codes can be merged. If this is not the case, put WIP (Work in progress) in the title of merge request.
 - Choose "Squash Commit" in the merge request so that the commit history will be set invisible.
 - Always use merge request to update work, so that the working history is tracable.

### Environment File
- The environment file in the `main` branch should always be synchronized with the program and should be updated simultaneously with other files.
- In order to keep the used third-party packages in a reasonable size, it is necessary to stay conservative when adding new packages.

### Modeling Language
The currently used modeling language is:
> pyomo: https://pyomo.readthedocs.io/en/stable/index.html

### Programming Guidelines and Coding Style
All developers are kindly asked to follow the google style guide for Python: https://google.github.io/styleguide/pyguide.html

## Project Structure
The project has one main repository (FOCUS-framework) and multiple sub-repositories:

### Repository FOCUS-framework
The FOCUS-framework repository is the framework's main repository. It contains the executable scripts and data that is required to run example scenarios. 

  - `runme.py` : Main script to build a prosumer model and run the optimization process and its subfuctions
  - `LICENSE` :	File containing the license description
  - `envINEED.yml` : YAML-File listing the required Python packages
  - `input_files/` : Directory for input file examples
  - `output_files/` : Directory to save results from optimization
  
### Sub-repository Model_Library
The sub-repository Model_Libary contains the mathematical models of component, prosumer, quarter, and city levels. 

  - `Components/` : Contains all component models and initialized pre-configured components
  - `Prosumer/` : Includes Prosumer model whhich allows to flexibily model any Prosumer application
  - `District/` : Will contain district level model (implementation in progress).
  - `City/` : Will contain city level model (implementation in progress).

### Sub-repository Tooling
The tooling sub-repository gathers all tools that are independent from the main optimization and provide additional functionality.

  - `aggregator/` : Tool to aggregate time series.
  - `demand_generator/` : Tool to generate generic demand time series based on the oemof demand generator.
  - `error_estimator/` : Tool to estimate .
  - `predictor/` : Tool to predict time series for rolling horizon simulation.
  - `price_crawler/` : Tool to gather price data of components.
  - `results evaluation/` : Tool to evaluate single household prosumer application results.
  - `sensitivity_analysis_sector_coupling/` : Tool to aevaluate sector coupling results.

### Sub-repository GUI
The GUI sub-repository includes a graphical user interface that acts as an executable of the Framework (currently in development). 

### Sub-repository Database_connector
The Database_conector sub-repository provides the database structure, in which all data for optimization/simulation is stored (currently in development).

## Copyright
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## Acknowledgements
This work was supported by the funding initiative of "FEN Research Campus - Public-Private Partnership for Innovation" from the Federal Ministry of Education and Research (BMBF). The framework is being developed within its project "InEEd-DC", funding no. 03SF0597.
