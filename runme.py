"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pandas as pd
import Tooling.input_profile_processor.input_profile_processor
from Tooling.dynamics.Dynamic import GlobalDynamic
import Model_Library.Prosumer.main as main_prosumer
from enum import Enum
import json

class SimulationScope(Enum):
    PROSUMER = 1
    DISTRICT = 2

simulation_scope = SimulationScope.PROSUMER
t_start = pd.Timestamp("2019-06-06 00:00:00") # start time of simulation
global_dynamic = GlobalDynamic([900 for i in range(1344)]) ##900=15min resolution ## 1344= 2 weeks
dynamic = global_dynamic.root()

input_profile_dict = {'irradiance_1': {'type': 'irradiance', 'file': 'input_files/data/irradiance/Lindenberg2006BSRN_Irradiance_60sec.csv'},
                      'temperature_1': {'type': 'air_temperature', 'file': 'input_files/data/temperature/temperature.csv'},
                      'demand_electric_1': {'type': 'elec_demand', 'generate': {'yearly_demand': 5000}},
                      'demand_electric_2': {'type': 'elec_demand', 'file': 'input_files/data/demand/electricity/LoadProfile.csv'},
                      'elec_price_1': {'type': 'elec_price', 'file': 'input_files/data/prices/day-ahead/hourly_price.csv'},
                      'elec_price_2': {'type': 'elec_price', 'file': 'input_files/data/prices/intra_day/intra-day_price_2022.csv'},
                      'EV_profile_1': {'type': 'EV_profile', 'file': 'input_files/data/demand/mobility/Profile_one_year_home22kW_0-24_VW_ID.3_updated.csv'}}

input_profiles = Tooling.input_profile_processor.input_profile_processor.process_input_profiles(input_profile_dict, t_start, dynamic)

prosumer_paths = {'V2H_Prosumer': 'input_files/models/prosumer_models/V2H_prosumer/prosumer.json'}
prosumer_profiles = {'V2H_Prosumer': {'pv_roof': {'irradiance' : 'irradiance_1', 'temperature': 'temperature_1'},
                                                  'elec_cns': {'consumption': 'demand_electric_1'},
                                                  'drv_cns': {'consumption': 'EV_profile_1'},
                                                  'grd': {'price': 'elec_price_1'}}}

prosumer_dict = dict()
for prosumer_name, prosumer_path in prosumer_paths.items():
    with open(prosumer_path) as f:
        prosumer_json = json.load(f)
    prosumer_dict[prosumer_name] = prosumer_json
for prosumer_name, component_profiles in prosumer_profiles.items():
    for component_name, profiles in component_profiles.items():
        prosumer_dict[prosumer_name]['components'][component_name].update(profiles)

prosumer_main = main_prosumer.ProsumerMain(prosumer_dict, input_profiles, dynamic)

prosumer_sizing_strategy = ['annuity']
prosumer_main.optimize_sizing('sized', prosumer_sizing_strategy, input_profiles)

prosumer_main.save_results()

prosumers = prosumer_main.prosumers

if simulation_scope == SimulationScope.PROSUMER:
    exit()
    