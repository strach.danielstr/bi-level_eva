"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This script inspects the input files and modifies them such that they adhere to the new input file specification used by the framework after merge of merge request !12 Refactoring Part 1.
"""

import os.path
import pandas as pd

def read_matrix(df):
    matrix = []
    for i in df.index:
        matrix_row = []
        for j in df.index:
            matrix_row.append(df[df["comp_name"][j]][i])
        matrix.append(matrix_row)
    return matrix

def read_components(df):
    components = {}
    for i in df.index:
        components[df["comp_name"][i]] = i
    return components

def get_connection(matrix, components, comp_from, comp_to):
    return matrix[components[comp_from]][components[comp_to]]

def set_connection(matrix, components, comp_from, comp_to, value):
    matrix[components[comp_from]][components[comp_to]] = value

def write_matrix(df, matrix):
    for i in df.index:
        for j in df.index:
            df.loc[i, df["comp_name"][j]] = matrix[i][j]

changed_files = []
invalid_files = []
for dirpath, dirnames, filenames in os.walk(".\\input_files"):
    for filename in filenames:
        matrix_names = ["elec_matrix.csv", "gas_matrix.csv", "hydro_matrix.csv", "therm_matrix.csv"]
        consumption_component_names = ["ChargingInfrastructure", "StandardElectricalConsumption", "HeadConsumption", "Radiator", "HotWaterConsumption"]
        if filename.find('matrix') and filename.endswith('.csv') and (filename.find('elec') is not -1 or filename.find('gas') is not -1 or filename.find('hydro') is not -1 or filename.find('therm') is not -1):
            try:
                print(f"Inspecting file {os.path.join(dirpath, filename)}")
                df = pd.read_csv(os.path.join(dirpath, filename))
                file_changed = False
                matrix = read_matrix(df)
                components = read_components(df)
                for component in components.keys():
                    if df["comp_type"][components[component]] in consumption_component_names:
                        receives_from = []
                        gives_to = []
                        for other_component in components.keys():
                            if get_connection(matrix, components, other_component, component) == 1:
                                receives_from.append(other_component)
                            if get_connection(matrix, components, component, other_component) == 1:
                                gives_to.append(other_component)
                        if len(gives_to) != 0:
                            file_changed = True
                            for recieving_component in gives_to:
                                set_connection(matrix, components, component, recieving_component, 0)
                                for giving_component in receives_from:
                                    set_connection(matrix, components, giving_component, recieving_component, 1)
                if file_changed:
                    changed_files.append(os.path.join(dirpath, filename))
                    write_matrix(df, matrix)
                    df.to_csv(os.path.join(dirpath, "temp.csv"), index = False)
                    df_str = pd.read_csv(os.path.join(dirpath, "temp.csv"), dtype = str, keep_default_na = False)
                    for i in df.columns:
                        if issubclass(type(df[i][0]), float):
                            for j in df.index:
                                if len(df_str[i][j]) != 0 and df_str[i][j].endswith(".0"):
                                    df_str[i][j] = f"{df[i][j]:.0f}"
                    df_str.to_csv(os.path.join(dirpath, filename), index = False)
                    os.remove(os.path.join(dirpath, "temp.csv"))
            except KeyError:
                invalid_files.append(os.path.join(dirpath, filename))
for file in changed_files:
    print(f"Modified file {file}!")
for file in invalid_files:
    print(f"File {file} breaks some part of the input file specification that during the refactoring!")
