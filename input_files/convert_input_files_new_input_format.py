"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This script inspects the input files and modifies them such that they adhere to the new input file specification used by the framework after merge of merge request !24 New input format.
"""

import os.path
import pandas as pd
import numpy as np
import json

def parse_component_config(component_name, component_type, config, component_json):
    possible_keys = dict()
    if component_type == 'CoolGrid':
        possible_keys['price'] = component_name + '_price'
        possible_keys['injection_price'] = component_name + '_injection_price'
    elif component_type == "ElectricalGrid":
        possible_keys['price'] = component_name + '_price'
        possible_keys['injection_price'] = component_name + '_injection_price'
        possible_keys['emission'] = component_name + '_emission'
        possible_keys['peak_power_cost'] = component_name + '_peak_power_costs'
    elif component_type == "GasGrid":
        possible_keys['price'] = component_name + '_price'
        possible_keys['injection_price'] = component_name + '_injection_price'
        possible_keys['peak_power_cost'] = component_name + '_emission'
    elif component_type == 'HeatGrid':
        possible_keys['price'] = component_name + '_price'
        possible_keys['injection_price'] = component_name + '_injection_price'
    for key, prefixed_key in possible_keys.items():
        if prefixed_key in config:
            component_json[key] = config[prefixed_key]
            del config[prefixed_key]

def np_encoder(object):
    if isinstance(object, np.generic):
        return object.item()

changed_topologies = []
invalid_topologies = []
for dirpath, dirnames, filenames in os.walk(".\\input_files"):
    components_here = False
    config_here = False
    connections_here = False
    for filename in filenames:
        if filename == "components.csv":
            components_file = filename
            components_here = True
        elif filename == "config.csv":
            config_file = filename
            config_here = True
        elif filename == "connections.csv":
            connections_file = filename
            connections_here = True
    
    if components_here and config_here and connections_here:
        try:
            print(f"Inspecting topology {dirpath}")
            components_df = pd.read_csv(os.path.join(dirpath, components_file))
            components = {}
            for i in components_df.index:
                components[components_df["name"][i]] = components_df.loc[i]
            config_df = pd.read_csv(os.path.join(dirpath, config_file))
            config_dict = config_df.to_dict(orient='list')
            for i in config_dict:
                config_dict[i] = config_dict[i][0]
            connections = pd.read_csv(os.path.join(dirpath, connections_file))
            prosumer_json = dict()
            components_json = dict()
            for component in components.values():
                component_json = dict()
                for i in component.index:
                    if i == 'name' or i == 'current_size' or isinstance(component[i], np.float) and np.isnan(component[i]):
                        continue
                    component_json[i] = component[i]
                parse_component_config(component['name'], component['type'], config_dict, component_json)
                components_json[component['name']] = component_json
            prosumer_json['components'] = components_json
            connections_json = []
            for i in connections.index:
                connection_json = dict()
                connection_json['from'] = connections['from'][i]
                connection_json['output'] = connections['output'][i]
                connection_json['to'] = connections['to'][i]
                connection_json['input'] = connections['input'][i]
                connections_json.append(connection_json)
            prosumer_json['connections'] = connections_json
            for key, value in config_dict.items():
                prosumer_json[key] = value
            changed_topologies.append(dirpath)
            os.remove(os.path.join(dirpath, "components.csv"))
            os.remove(os.path.join(dirpath, "config.csv"))
            os.remove(os.path.join(dirpath, "connections.csv"))
            with open(os.path.join(dirpath, "prosumer.json"), "w", encoding ="utf-8") as f:
                json.dump(prosumer_json, f, indent = 4, default = np_encoder)
        except:
            invalid_topologies.append(dirpath)
for directory in changed_topologies:
    print(f"Modified topology {directory}!")
for file in invalid_topologies:
    print(f"Topology {file} breaks some part of the input file specification!")
    
changed_models = []
invalid_models = []
for dirpath, dirnames, filenames in os.walk(".\\Model_Library\\Component\\data"):
    for filename in filenames:
        if filename.endswith(".csv"):
            try:
                print(f"Inspecting model {os.path.join(dirpath, filename)}")
                model = pd.read_csv(os.path.join(dirpath, filename))
                model_json = dict()
                for i in model.columns:
                    no_space_name = "_".join(i.split(" "))
                    model_json[no_space_name] = model[i][0]
                changed_models.append(os.path.join(dirpath, filename))
                os.remove(os.path.join(dirpath, filename))
                with open(os.path.join(dirpath, "".join(filename.split(".")[:-1]) + ".json"), "w", encoding ="utf-8") as f:
                    json.dump(model_json, f, indent = 4, default = np_encoder)
            except:
                invalid_models.append(os.path.join(dirpath, filename))
for directory in changed_models:
    print(f"Modified model {directory}!")
for file in invalid_models:
    print(f"Model {file} breaks some part of the input file specification!")
