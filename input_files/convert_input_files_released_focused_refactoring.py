"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This script inspects the input files and modifies them such that they adhere to the new input file specification used by the framework after merge of merge request !13 Release focused refactoring.
"""

import os.path
import pandas as pd

def component_to_commodities(type):
    if type == "AbsorptionChiller":
        return "heat", None, "cold", None
    if type == "CompressorChiller":
        return "electricity", None, "cold", None
    if type == "CoolConsumption":
        return "cold", None, None, None
    if type == "CoolGrid":
        return "cold", None, "cold", None
    if type == "ChargingInfrastructure":
        return "electricity", None, None, None
    if type == "ElectricalBusBar":
        return "electricity", None, "electricity", None
    if type == "ElectricalConsumption":
        return "electricity", None, None, None
    if type == "ElectricalGrid":
        return "electricity", None, "electricity", None
    if type == "PVGenerator":
        return None, None, "electricity", None
    if type == "DynamicInverter":
        return "electricity", None, "electricity", None
    if type == "StaticInverter":
        return "electricity", None, "electricity", None
    if type == "LiionBattery":
        return "electricity", None, "electricity", None
    if type == "BHKW":
        return "gas", None, "heat", "electricity"
    if type == "CHP":
        return "gas", None, "heat", "electricity"
    if type == "GasGrid":
        return "gas", None, "gas", None
    if type == "ElectricBoiler":
        return "electricity", None, "heat", None
    if type == "GasBoiler":
        return "gas", None, "heat", None
    if type == "HeatGrid":
        return "heat", None, "heat", None
    if type == "ElectricRadiator":
        return "electricity", None, "heat", None
    if type == "HeatConsumption":
        return "heat", None, None, None
    if type == "Radiator":
        return "heat", None, None, None
    if type == "UnderfloorHeat":
        return "heat", None, "heat", None
    if type == "HeatExchanger":
        return "heat", None, "heat", None
    if type == "GasHeatPump":
        return "gas", None, "heat", None
    if type == "HeatPump":
        return "electricity", None, "heat", None
    if type == "HotWaterConsumption":
        return "heat", None, None, None
    if type == "SolarThermalCollector":
        return None, None, "heat", None
    if type == "HotWaterStorage":
        return "heat", None, "heat", None
    if type == "PEMElectrolyzer":
        return "electricity", None, "hydrogen", None
    if type == "PEMFuelCell":
        return "hydrogen", None, "electricity", "heat"
    if type == "PEMElectrolyzer":
        return "electricity", None, "hydrogen", None
    if type == "PressureStorage":
        return "hydrogen", None, "hydrogen", None
    raise KeyError

changed_topologies = []
invalid_topologies = []
for dirpath, dirnames, filenames in os.walk(".\\input_files"):
    components_here = False
    connections_here = False
    for filename in filenames:
        if filename == "components.csv":
            components_file = filename
            components_here = True
        elif filename == "connections.csv":
            connections_file = filename
            connections_here = True
    
    if components_here and connections_here:
        try:
            print(f"Inspecting topology {dirpath}")
            components_df = pd.read_csv(os.path.join(dirpath, components_file))
            components = {}
            for i in components_df.index:
                components[components_df["name"][i]] = components_df.loc[i]
            connections = pd.read_csv(os.path.join(dirpath, connections_file))
            connections['output'] = pd.Series("", index = connections.index)
            connections['input'] = pd.Series("", index = connections.index)
            for i in connections.index:
                commodity = connections['sector'][i]
                connection_from = connections['from'][i]
                connection_to = connections['to'][i]
                from_type = components[connection_from]['type']
                input_commodity_1, input_commodity_2, output_commodity_1, output_commodity_2 = component_to_commodities(from_type)
                if commodity == output_commodity_1:
                    connections['output'][i] = "1"
                elif commodity == output_commodity_2:
                    connections['output'][i] = "2"
                else:
                    raise KeyError
                to_type = components[connection_to]['type']
                input_commodity_1, input_commodity_2, output_commodity_1, output_commodity_2 = component_to_commodities(to_type)
                if commodity == input_commodity_1:
                    connections['input'][i] = "1"
                elif commodity == input_commodity_2:
                    connections['input'][i] = "2"
                else:
                    raise KeyError
            connections.drop("sector", axis = 1)
            connections = connections[["from", "output", "to", "input"]]
            changed_topologies.append(dirpath)
            connections.to_csv(os.path.join(dirpath, "connections.csv"), index = False)
        except:
            invalid_topologies.append(dirpath)
for directory in changed_topologies:
    print(f"Modified topology {directory}!")
for file in invalid_topologies:
    print(f"Topology {file} breaks some part of the input file specification!")
